#include <iostream> //cout
#include <stdio.h> //printf

#include <string> //string
#include <sys/socket.h> //socket
#include <arpa/inet.h> //inet_addr
#include <netdb.h> //hostent

using namespace std;



class TcpClient{

       private:
        int _sock;
        string _address;
        string response_data = "";
        int _port;
        struct sockaddr_in serv_addr;
    
        
        public:
        void set_tcp_ip_and_port(std::string ip, int port);
        TcpClient();
        bool sending(const char* data);
        void continuous_receive();
        bool connecting();



};