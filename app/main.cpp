#include <iostream>
#include "TcpClient.h"

using namespace std;

int main(){

    TcpClient tcp;
    string ip;
    int port;
    cout<<"Enter IP to connect with"<<endl;
    cin>>ip;
    cout<<"Enter port to connect with"<<endl;
    cin>>port;

    tcp.set_tcp_ip_and_port(ip,port);


    if(tcp.connecting()!=0){
        exit(0);
    }
    char* message="Client says Hii";
    tcp.sending(message);

    tcp.continuous_receive();
    



    return 0;
}