#include <iostream> //cout
#include <stdio.h> //printf
#include <string.h> //strlen
#include <string> //string
#include <sys/socket.h>//socket
#include <arpa/inet.h> //inet_addr
#include <netdb.h> //hostent
#include <unistd.h>
#include "TcpClient.h"
using namespace std;
#define PORT 8080

TcpClient::TcpClient()
{
    _sock = -1;
    _port = 0;
    _address = "";
}

void TcpClient::set_tcp_ip_and_port(std::string ip, int port){
    _address=ip;
    _port= port;

}

bool TcpClient::connecting(){
  //Create socket if not created already
  cout<<"CLIENT"<<endl;
  try{
      if(_sock==-1){
          _sock= socket( AF_INET, SOCK_STREAM,0);
      }

      if(_sock<0){
        throw(_sock);
      }
      else{
          
          cout<<"Socket Creation Successful"<<endl;
      }
  }  
  catch(int _sock ){
  cout<<"Socket Creation Unsuccessful"<<endl;  
  cout<<strerror(errno)<<endl;
  }

/*
    if(_sock==-1){
      //Step1: Create socket for first time
      //int socket(domain, stream type sock/datagram, protocol)
      
      _sock= socket( AF_INET, SOCK_STREAM,0);


      if(_sock<0){
          printf("Socket not created");
          //return 1;
      }
      cout<<"Socket Created"<<endl;
  }*/
    
     //setup _address-connect() system call establishes a connection to the server.
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(_port); //decides edian

    //inet_addr(_server_ip_addr.c_str());
    //?? Didnt understand what this id doing
    // char *ip;
    cout<<"here1"<<endl;
    //strcpy(ip, _address.c_str());
    cout<<"here2"<<endl;
    //char *ip= _address.c_str()
    if(inet_pton(AF_INET, _address.c_str(), &serv_addr.sin_addr)<=0) 
    {
        printf("\nInvalid address/ Address not supported \n");
        return false;
    }
     cout<<"\ncall connect function\n";
     try{
         if(connect(_sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == 0){
             cout<<"Server Connection Successful"<<endl;
             return 0;
         }
         else{
             throw(connect(_sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)));
         }
     }
     catch(int status){
         cout<<"Server Connection Unsuccessful"<<endl;  
         cout<<"Error: "<< strerror(errno)<<endl;
         return -1;
     }

}
    /*
     if (connect(_sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection  with Server Failed \n");
        return false;
    }
     cout<<" Connection with Server Successful"<<endl;
     return 1; 
}

*/
bool TcpClient::sending(const char* data){
    //char* data;
    //data="Hii from Client";
    
    cout<<"Function called to send data"<<endl;
    //char *hello = "client says Hii";
    int size= strlen(data);
    //cout<< size<<endl;

    try{
        if(send(_sock, data, size ,0)!=-1){
            cout<<"Data Sent by Client Successfully"<<endl;
        }
        else{
            throw(send(_sock, data, size ,0));
        }
    }
    catch(int status){
        cout<<"Data Unable to send by Client- Unsuccessful"<<endl;
        cout<<"Error: "<< strerror(errno)<<endl;
    }
}
    /*
    if(send(_sock, data, size ,0)<0){
        cout<<"\nUnable to send data\n";
        return false;

    }

    cout<<"\nData Sent to Server\n"<<endl;
    return true;
}*/

void TcpClient::continuous_receive(){
    //int size=1024;
    cout<<"Function called to receive data"<<endl;
    char buffer[1024]={0};
    int reply;
    
    //Receive replies from server
    while(reply>0)
    reply= read(_sock, buffer, 1024);
    cout<<"here  "<<reply<<endl;
       cout<< buffer<<endl;
       
    }



}

